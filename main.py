import time

from tgtg import TgtgClient
import json
import requests

def send_notification(prickle):
    return requests.post(
        "https://api.mailgun.net/v3/API_DOMAIN/messages",
        auth=("api", "KEY"),
        data={"from": "Excited User <mailgun@DOMAIN.mailgun.org>",
              "to": ["your@email.com"],
              "subject": "TOOGOODTOGO",
              "text": prickle})
# fetch your creds like this and plug into the function below
# client = TgtgClient(email="<your_email>")
# credentials = client.get_credentials()
client = TgtgClient(
    access_token="ACCESS",
    refresh_token="REFRESH",
    user_id="ID")
latest_values = {};
while True:
    for item in client.get_items(favorites_only=True):
        if (item['items_available'] != 0 and
                (item['display_name'] not in latest_values or latest_values[item['display_name']] != item[
                    'items_available'])):
            print('got one')
            print(latest_values)
            send_notification(item['display_name'] + " is now available GO GO GO");

        else:
            print("got nothing");
        latest_values[item['display_name']] = item['items_available']
    time.sleep(60)
